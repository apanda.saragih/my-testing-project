from django import forms
from .models import StatusModel


class StatusForm(forms.ModelForm):
	status = forms.CharField(max_length=300, widget=forms.Textarea)
	class Meta:
		model = StatusModel
		fields= ('status',)
